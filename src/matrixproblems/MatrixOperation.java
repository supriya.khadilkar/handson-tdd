package matrixproblems;

public class MatrixOperation
{
    public int[][] multiplyMatrices(int[][] mat1, int[][] mat2)
    {
        int[][] resultMat = new int[1][1];
        int secondMatRows = mat2.length;
        int firstMatRows = mat1.length;
        int resultMatRows = 0, resultMatCols =0;
        int resultCols = 0, secondMatCols = 0;
        int firstMatCols = 0;
        if(firstMatRows > 0 && secondMatRows > 0)
        {
            firstMatCols = mat1[0].length;
            secondMatCols = mat2[0].length;

            if(firstMatCols > 0 && secondMatCols >0)
            {
                resultMat = new int[firstMatRows][secondMatCols];
                for(int row = 0; row < firstMatRows; row++)
                {
                    for(int col = 0; col < secondMatCols; col++)
                    {
                        for(int delta = 0; delta < firstMatCols; delta++)
                        {
                            resultMat[row][col] = resultMat[row][col] + (mat1[row][delta] * mat2[delta][col]);
                        }
                    }
                }
            }
        }
        return resultMat;
    }
    public int[][] addMatrices(int[][] mat1, int[][] mat2)
    {
        int[][] resultMat;
        if(mat1.length == 0 && mat2.length == 0)
            return new int[1][1];
        else
        {
            int firstMatrixRows = mat1.length;
            int secondMatrixRows = mat2.length;
            int firstMatrixCols = 0;
            int secondMatrixCols = 0;
            if((firstMatrixRows-secondMatrixRows) != 0)
                return new int[1][1];
            else
            {
                firstMatrixCols = mat1[0].length;
                secondMatrixCols = mat2[0].length;
                System.out.println(firstMatrixCols);
                if((firstMatrixCols - secondMatrixCols) != 0)
                    return  new int[1][1];
                else
                {
                    resultMat = new int[firstMatrixRows][firstMatrixCols];
                    for(int row = 0; row < firstMatrixRows; row++)
                    {
                        for(int col = 0; col < firstMatrixCols; col++)
                        {
                            resultMat[row][col] = mat1[row][col] + mat2[row][col];
                        }
                    }
                }
            }
        }
        return resultMat;
    }

    /*public static void main(String[] args)
    {
        MatrixOperation matrixOps = new MatrixOperation();
        int[][] mat1 = new int[2][2];
        int[][] mat2 = new int[2][2];

        mat1[0][0] = 1;
        mat1[0][1] = 2;
        mat1[1][0] = 3;
        mat1[1][1] = 4;

        mat1[0][0] = 5;
        mat1[0][1] = 6;
        mat1[1][0] = 7;
        mat1[1][1] = 8;

        int[][] resultMap = matrixOps.addMatrices(mat1, mat2);
        for(int i = 0; i < resultMap.length; i++)
        {
            for(int j = 0; j < resultMap[0].length; j++)
            {
                System.out.print(resultMap[i][j] + "\t");
            }
            System.out.println();
        }
      //  System.out.println(resultMap);

    }*/
}
