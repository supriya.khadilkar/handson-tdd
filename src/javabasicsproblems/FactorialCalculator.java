package javabasicsproblems;

/**
 * Java Basic Hands-on: #5
 */
public class FactorialCalculator
{
    /*
        considerations:
        For negative input throw exception
        range of input
     */

    public long calculateFactorial(int input)
    {
        int fact = 1;
        if(input > 0) {
            for (int i = 1; i <= input; i++) {
                fact = fact * i;
            }
        }
        return fact;
    }

   /* public static void main(String[] args) {
        int input = 5;
        System.out.println(calculateFactorial(input));

        input = 7;
        System.out.println(calculateFactorial(input));
    }*/
}
