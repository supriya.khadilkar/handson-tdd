package javabasicsproblems;

import java.util.ArrayList;

public class Fibonacci
{
    /**
     *
     * Java Basics hands-on #1
     * Fibonacci
     */

    public ArrayList<Long> calculateSeriesForNofTerms(int inputTerms)
    {
        ArrayList<Long> series = new ArrayList<>();
        if(inputTerms == 1)
            series.add(0l);
        else if(inputTerms == 2)
        {
            series.add(0l);
            series.add(1l);
        }
        else
        {
            series.add(0l);
            series.add(1l);
            int terms = 2;
            long num1 = 0, num2 =1, sum = num1 + num2;
            while(terms <= inputTerms)
            {
                series.add(sum);
                num1 = num2;
                num2 = sum;
                sum = num1 + num2;
                terms++;
            }
        }
        return  series;
    }
}
