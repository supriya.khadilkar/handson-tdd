package javabasicsproblems;

public class PalindromeChecker
{

    /* case-sensitive logic*/
    public boolean checkPalindrome(String input)
    {
        boolean isPalindrome = true;
        if(input.isEmpty())
            isPalindrome = false;
        else if(input.length() == 1)
            isPalindrome = true;
        else
        {
            int start = 0; int end = input.length() - 1;
            int mid = (start + end) / 2;
            for(;start<mid && end>mid; start++, end--)
            {
                if(input.charAt(start) != input.charAt(end)) {
                    isPalindrome = false;
                    break;
                }
            }
        }
        return isPalindrome;
    }
}
