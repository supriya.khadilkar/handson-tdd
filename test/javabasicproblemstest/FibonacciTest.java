package javabasicproblemstest;

import javabasicsproblems.Fibonacci;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

public class FibonacciTest
{
    @Test
    public void shouldReturnFibonnaciSeriesGivenNoOfTerms1()
    {
        Fibonacci fibonacci = new Fibonacci();
        ArrayList<Long> expectedFibSeries = new ArrayList<>();
        expectedFibSeries.add(0l);
        int input  = 1;
        ArrayList<Long> resultFibSeries = fibonacci.calculateSeriesForNofTerms(input);
        Assert.assertEquals(expectedFibSeries, resultFibSeries);
    }

    @Test
    public void shouldReturnFibonnaciSeriesGivenNoOfTerms2()
    {
        Fibonacci fibonacci = new Fibonacci();
        ArrayList<Long> expectedFibSeries = new ArrayList<>();
        expectedFibSeries.add(0l);
        expectedFibSeries.add(1l);
        int input  = 2;
        ArrayList<Long> resultFibSeries = fibonacci.calculateSeriesForNofTerms(input);
        Assert.assertEquals(expectedFibSeries, resultFibSeries);
    }

    @Test
    public void shouldReturnFibSeriesGivenNoOfTerms3()
    {
        Fibonacci fibonacci = new Fibonacci();
        ArrayList<Long> expectedFibSeries = new ArrayList<>();
        expectedFibSeries.add(0l);
        expectedFibSeries.add(1l);
        expectedFibSeries.add(1l);
        int input  = 3;
        ArrayList<Long> resultFibSeries = fibonacci.calculateSeriesForNofTerms(input);
        //Assert.assertEquals(expectedFibSeries, resultFibSeries);
    }

    @Test
    public void shouldReturnFibSeriesGivenNoOfTerms5()
    {
        Fibonacci fibonacci = new Fibonacci();
        ArrayList<Long> expectedFibSeries = new ArrayList<>();
        expectedFibSeries.add(0l);
        expectedFibSeries.add(1l);
        expectedFibSeries.add(1l);
        expectedFibSeries.add(2l);
        expectedFibSeries.add(3l);
        int input  = 5;
        ArrayList<Long> resultFibSeries = fibonacci.calculateSeriesForNofTerms(input);
        //Assert.assertEquals(expectedFibSeries, resultFibSeries);
    }

    @Test
    public void shouldReturnFibSeriesGivenNoOfTerms15()
    {
        Fibonacci fibonacci = new Fibonacci();
        ArrayList<Long> expectedFibSeries = new ArrayList<>();
        expectedFibSeries.add(0l);
        expectedFibSeries.add(1l);
        expectedFibSeries.add(1l);
        expectedFibSeries.add(2l);
        expectedFibSeries.add(3l);
        expectedFibSeries.add(5l);
        expectedFibSeries.add(8l);
        expectedFibSeries.add(13l);
        expectedFibSeries.add(21l);
        expectedFibSeries.add(34l);
        expectedFibSeries.add(55l);
        expectedFibSeries.add(89l);
        int input  = 12;
        ArrayList<Long> resultFibSeries = fibonacci.calculateSeriesForNofTerms(input);
        //Assert.assertEquals(expectedFibSeries, resultFibSeries);
    }

}
