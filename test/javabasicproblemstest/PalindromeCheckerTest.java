    package javabasicproblemstest;

    import javabasicsproblems.PalindromeChecker;
    import org.junit.Assert;
    import org.junit.Test;

    public class PalindromeCheckerTest
    {

        @Test
        public void shouldReturnTrueGivenStringContainsOnlyOneChar()
        {
            PalindromeChecker checker = new PalindromeChecker();
            String input = "A";
            boolean result = checker.checkPalindrome(input);
            Assert.assertEquals(true, result);
        }

        @Test
        public void shouldReturnFalseForEmptyString()
        {
            PalindromeChecker checker = new PalindromeChecker();
            String input = "";
            boolean result = checker.checkPalindrome(input);
            Assert.assertEquals(false, result);

        }

        @Test
        public void shouldReturnTrueForEvenLengthString()
        {
            PalindromeChecker checker = new PalindromeChecker();
            String input = "ABBA";
            boolean result = checker.checkPalindrome(input);
            Assert.assertEquals(true, result);
        }

        @Test
        public void shouldReturnTrueForOddLengthString()
        {
            PalindromeChecker checker = new PalindromeChecker();
            String input = "ABA";
            boolean result = checker.checkPalindrome(input);
            Assert.assertEquals(true, result);
        }

        @Test
        public void shouldReturnFalseGivenStringisNotPalindorme()
        {
            PalindromeChecker checker = new PalindromeChecker();
            String input = "aabbbssww";
            boolean result = checker.checkPalindrome(input);
            Assert.assertEquals(false, result);
        }

        @Test
        public void shouldReturnTrueGivenStringIsPalindorme()
        {
            PalindromeChecker checker = new PalindromeChecker();
            String input = "madam";
            boolean result = checker.checkPalindrome(input);
            Assert.assertEquals(true, result);
        }

    }
