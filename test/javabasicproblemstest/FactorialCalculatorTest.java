package javabasicproblemstest;

import javabasicsproblems.FactorialCalculator;
import org.junit.Assert;
import org.junit.Test;

public class FactorialCalculatorTest
{

    @Test
    public void shouldReturnFactorialasOneGivenInputisZero()
    {
        FactorialCalculator fc = new FactorialCalculator();
        long result = fc.calculateFactorial(0);
        Assert.assertEquals(1, result);
    }

    @Test
    public void shouldReturnCalculatedFactorialForAnyPositiveNo()
    {
        FactorialCalculator fc = new FactorialCalculator();
        long result = fc.calculateFactorial(7);
        Assert.assertEquals(5040, result);
    }
}
