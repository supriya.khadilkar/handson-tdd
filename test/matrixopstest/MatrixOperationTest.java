package matrixopstest;

import matrixproblems.MatrixOperation;
import org.junit.Assert;
import org.junit.Test;

public class MatrixOperationTest
{
    @Test
    public void shouldReturnEmptyMatrixGivenEmptyMatrix()
    {
        int[][] mat1 = new int[1][1];
        int[][] mat2 = new int[1][1];
        int[][] result = new int[1][1];
        int[][] expected = new int[1][1];

        MatrixOperation mxOps = new MatrixOperation();
        result = mxOps.addMatrices(mat1, mat2);
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void shouldReturnEmptyMatrixGivenUnEvenMatrixDimensions()
    {
        int[][] mat1 = new int[2][1];
        int[][] mat2 = new int[1][3];
        int[][] result = new int[1][1];
        int[][] expected = new int[1][1];

        MatrixOperation mxOps = new MatrixOperation();
        result = mxOps.addMatrices(mat1, mat2);
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void shouldReturnResultantMatrixGivenSquareShapedMatrix()
    {
        int[][] mat1 = new int[2][2];
        int[][] mat2 = new int[2][2];
        int[][] result = new int[2][2];
        int[][] expected = new int[2][2];

        int cols = mat1[0].length;
        int delta = 1;
        mat1[0][0] = 1;
        mat1[0][1] = 2;
        mat1[1][0] = 3;
        mat1[1][1] = 4;

        mat2[0][0] = 5;
        mat2[0][1] = 6;
        mat2[1][0] = 7;
        mat2[1][1] = 8;

        expected[0][0] = 6;
        expected[0][1] = 8;
        expected[1][0] = 10;
        expected[1][1] = 12;

        MatrixOperation mxOps = new MatrixOperation();
        result = mxOps.addMatrices(mat1, mat2);
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void shouldReturnEmptyFlightsGivenEmptyMatrices()
    {
        int[][] mat1 = new int[1][1];
        int[][] mat2 = new int[1][1];
        int[][] result = new int[1][1];
        int[][] expected = new int[1][1];

        MatrixOperation mxOps = new MatrixOperation();
        result = mxOps.multiplyMatrices(mat1, mat2);
        Assert.assertArrayEquals(expected, result);
    }

    @Test
    public void shouldReturnResultantMatrix()
    {
        int[][] mat1 = new int[2][3];
        int[][] mat2 = new int[3][2];

        mat1[0][0] = 1;
        mat1[0][1] = 5;
        mat1[0][2] = 6;
        mat1[1][0] = 2;
        mat1[1][1] = 3;
        mat1[1][2] = 4;

        mat2[0][0] = 1;
        mat2[0][1] = 5;
        mat2[1][0] = 2;
        mat2[1][1] = 7;
        mat2[2][0] = 3;
        mat2[2][1] = 8;

        int[][] result = new int[mat1.length][mat2[0].length];
        int[][] expected = new int[mat1.length][mat2[0].length];

        expected[0][0] = 29;
        expected[0][1] = 88;
        expected[1][0] = 20;
        expected[1][1] = 63;

        MatrixOperation mxOps = new MatrixOperation();
        result = mxOps.multiplyMatrices(mat1, mat2);
        Assert.assertArrayEquals(expected, result);

    }


}
